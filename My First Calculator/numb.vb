﻿Module numb
    Sub NumbericOnly(ByVal e As KeyPressEventArgs)
        If Not IsNumeric(e.KeyChar) And Not e.KeyChar = ChrW(Keys.Back) Then
            e.Handled = True
        End If
    End Sub
End Module
